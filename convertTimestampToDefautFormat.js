const convertTimestampToDefautFormat = (time: number) => {
    const date = new Date(time * 1000)
    let month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    const timeDefault = date.getDate() + '/' + month + '/' + date.getFullYear() + '-' + date.getHours() + ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':' + (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds())
    return timeDefault;
}
